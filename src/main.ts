import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as Queue from 'bull';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // bull board configurations:
  const { createBullBoard } = require('bull-board');
  // createBullBoard will be used to register our queues in the bull board for the GUI to show status
  const { BullAdapter } = require('bull-board/bullAdapter');

  const redisurl = 'redis://@localhost:6379';
  // this url will be used up for establishing the redis connection
  // In production, we need to use the redis-master's port and host and password
  // eg: 'redis://abc1232333@10.10.10.11:6379'

  console.log('THIS IS THE REDIS CONNECTION STRING', redisurl);

  const Redis = require('ioredis');
  // IO REDIS  is a npm package which help us to pass the connection params and other
  // optional arguments in the Redis connection instance

  const cluster = new Redis({ redisurl });
  // creating a redis connection object to be passed while creating
  // queues to be displayed on the GUI

  const Queue1 = new Queue('email', cluster);
  const Queue2 = new Queue('system', cluster);
  // Queue1 and Queue2 are the Queues which we must have IMPLEMENTED IN OUR BULLMQ
  // in production we pick these names usually from the declared enum where we have our queue names

  Queue1.on('error', function (error) {
    console.error(`Error in bull queue happend: ${error}`);
  }).on('failed', function (job, error) {
    console.error(`Task was failed with reason: ${error}`);
  });
  // example of some lifecycle methods which will be exected if our Queue trigger the lifecycle condition
  // eg: on error, on failed , on success etc.....

  const { router } = createBullBoard([
    new BullAdapter(Queue1),
    new BullAdapter(Queue2),
  ]);
  // the router object we get from the createBullBoard will be used in the
  // defining of the Bull Board URL

  app.use('/admin/queues', router);
  // Bull Board can be accessed above
  // we can have simple authentication for the Bull Board as the second argument to the app.use()
  /* EXAPMLE OF BASIC AUTH
      async (req, res, next) => {
      if (req.query.token) {
        const response = await cluster.get(req.query.token);
        if (response || true) {
          next();
        } else {
          res.json({ message: 'Unauth' });
        }
      }
    },


  */

  // bull board config above

  await app.listen(3000, () => console.log('app listening on port : 3000'));
}
bootstrap();
