import { Process, Processor } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';

// This is going to be our CONSUMER
// This shall run for every job we have added in our queue in our producer
// For defining a consumer we need to add the @Processor() decorator with the queue name as argument
@Processor('email')
export class EmailProcessor {
  private readonly logger = new Logger(EmailProcessor.name);

  // We can have multiple methods to server different jobs based on job names for the jobs in our queue
  // to make a method as a special method to be called for each job we need to add @Process() decorator with the optional job-name argument

  // process for 'send-email' job name
  @Process('send-email')
  handleJobLogic(job: Job) {
    this.logger.debug('Start sending email...');
    this.logger.debug(job.data);
    // we can do any heavy task here
    this.logger.debug('Email sent');
  }

  // process for 'something else' job name
  @Process('something-else')
  handleSomeOtherJobLogic(job: Job) {
    this.logger.debug('Start some logic...');
    this.logger.debug(job.data);
    // we can do any heavy task here
    this.logger.debug('Some other Logic executed');
  }
}
