import { InjectQueue } from '@nestjs/bull';
import { Controller, Post } from '@nestjs/common';
import { Queue } from 'bull';

@Controller('email')
export class EmailController {
  // we need to inject our queue in our constructor in order to use it
  // the @InjectQueue('email'), creates a queue reference for the supplied name,
  // Here Queue name is - 'email'
  constructor(@InjectQueue('email') private readonly emailQueue: Queue) {}

  // following function is going to add Jobs to our queue, Hence it is a PRODUCER in Bull terminology
  @Post('send-email')
  async SendEmail() {
    // the add method is used to add Jobs to the QUEUE.
    // first arg to the add method is the job name. we can write different consumers( node js method) to execute different operations based on the queue name
    // ex - do a send logic for the job name - 'send-email'
    // ex- dont do anything if job name is something else
    await this.emailQueue.add('send-email', {
      status: 'email sent',
    });
  }
}
