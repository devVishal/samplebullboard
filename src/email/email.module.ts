import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { EmailController } from './email.controller';
import { EmailProcessor } from './email.processor';

// we need to add the Bull queue to our module in order to register it
// we can use it registerQueue or registerQueueAsync for creating synch or aasynch queues

@Module({
  imports: [
    BullModule.registerQueue({
      name: 'email',
      // name param creates a new name with the supplied value,
      // it will be accessible throughout our app with the name attribute
    }),
  ],
  controllers: [EmailController],
  providers: [EmailProcessor],
})
export class EmailModule {}
